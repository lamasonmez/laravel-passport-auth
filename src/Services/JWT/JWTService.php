<?php


namespace Sonmez\LaravelPassportAuth\Services\JWT;


use Sonmez\LaravelPassportAuth\Services\JWT\IJWTService;
use Sonmez\LaravelPassportAuth\Services\Contracts\BaseService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
/**
 * Class UserService
 * @package App\Services\User
 */
class JWTService implements IJWTService
{

    public function __construct(){

    }
    /**
     * @return JsonResponse
     */
    public function getAuthenticatedUser()
    {
        return response()->json([
            'user_id' => Auth::user()->id,
            'status' => 'success',
        ]);
    }

    public function createTokenByCredentials(string $email, string $password): Object
    {
        $tokenRequest = Request::create(
            config('app.url') . config('auth.passport.token.link'),
            'POST',
            [
                'grant_type' => config('auth.passport.grant'),
                'client_id' => config('auth.passport.client.id'),
                'client_secret' => config('auth.passport.client.secret'),
                'username' => $email,
                'password' => $password,
                'scope' => '*'
            ]
        );
        app()->instance('request', $tokenRequest);
        $response = Route::dispatch($tokenRequest);

        return json_decode($response->getContent());
    }
}
