<?php

namespace Sonmez\LaravelPassportAuth;

use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Finder\Finder;

class PublishCommand extends Command
{
    protected $signature = 'laravel-passport-auth:publish {--force : Overwrite any existing files}';

    protected $description = 'Publish Package Controllers ';

    use ConfirmableTrait;

    public function handle()
    {
        if (! $this->confirmToProceed()) {
            return 1;
        }
        $this->publishAuthConfig();
        $this->info('Publishing Auth Config Done!');
        $this->publishControllers();
        $this->info('Publishing Controllers Done!');

        $this->publishMiddlewares();
        $this->info('Publishing Middlewares Done!');

        $this->publishApiRoutes();
        $this->info('Publishing Api Routes Done!');

        $this->publishEnv();
        $this->info('Publishing Env Variables Done!');

        $this->info('All done!');
    }
    private function publishControllers(){
        if (! is_dir($ApiPath =app_path('Http/Controllers/Api'))) {
            (new Filesystem)->makeDirectory($ApiPath);
        }
        if (! is_dir($AuthPath =app_path('Http/Controllers/Api/Auth'))) {
            (new Filesystem)->makeDirectory($AuthPath);
        }

        collect(File::files(__DIR__ . './Http/Controllers/Api/Auth'))->each(function (SplFileInfo $file) use ($AuthPath) {
            $sourcePath = $file->getPathname();

            $targetPath = $AuthPath . "/{$file->getFilename()}";

            if (! file_exists($targetPath) || $this->option('force')) {
                file_put_contents($targetPath, file_get_contents($sourcePath));
            }
        });

    }
    private function publishMiddlewares(){
        if (! is_dir($MiddelwarePath =app_path('Http/Middleware'))) {
            (new Filesystem)->makeDirectory($MiddelwarePath);
        }
        collect(File::files(__DIR__ . './Http/Middleware'))->each(function (SplFileInfo $file) use ($MiddelwarePath) {
            $sourcePath = $file->getPathname();

            $targetPath = $MiddelwarePath . "/{$file->getFilename()}";

            if (! file_exists($targetPath) || $this->option('force')) {
                file_put_contents($targetPath, file_get_contents($sourcePath));
            }
        });

    }
    private function publishApiRoutes(){
        if(File::isFile(base_path("routes/api.php"))){
            //get stub
            $api_stub =  file_get_contents(__DIR__ . '/../stubs/api.stub');
            file_put_contents(base_path('routes/api.php'),$api_stub,FILE_APPEND | LOCK_EX);
        }
    }
    private function publishAuthConfig(){
        // chagne the driver option to passport in api section
        file_put_contents(base_path('config/auth.php'), str_replace(
            "'driver' => 'token'",
            "'driver' => 'passport'",
            file_get_contents(base_path('config/auth.php'))
        ));
        //
         if(File::isFile(base_path("config/auth.php"))){
              //get stub
              $auth_config_stub =  file_get_contents(__DIR__ . '/../stubs/auth_config.stub');
              file_put_contents(base_path('config/auth.php'), str_replace(
                "];",
                $auth_config_stub .'];',
                file_get_contents(base_path('config/auth.php'))
            ));
         }
     }
     private function publishEnv(){
         if(File::isFile(base_path(".env"))){
             //get stub
             $api_stub =  file_get_contents(__DIR__ . '/../stubs/env.stub');
             file_put_contents(base_path('.env'),$api_stub,FILE_APPEND | LOCK_EX);
         }
     }
}
