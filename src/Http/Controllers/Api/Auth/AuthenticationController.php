<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Sonmez\LaravelPassportAuth\Services\JWT\IJWTService;




class AuthenticationController extends Controller
{
    protected $jwtService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(IJWTService $jwtService)
        {
            $this->jwtService = $jwtService;
        }

    public function validateToken(Request $request)
    {
        $result = $this->jwtService->getAuthenticatedUser();

        return response()
            ->json($result, 200);
    }
}
