<?php

namespace Sonmez\LaravelPassportAuth;
use Illuminate\Support\ServiceProvider;

use Sonmez\LaravelPassportAuth\Services\JWT\IJWTService;
use Sonmez\LaravelPassportAuth\Services\JWT\JWTService;

use Illuminate\Routing\Router;
use Sonmez\LaravelPassportAuth\Http\Middleware\Cors;
use Sonmez\LaravelPassportAuth\Http\Middleware\ForceJsonResponse;

class LaravelPassportAuthCServiceProvider extends ServiceProvider
{

    public function boot(){

        if ($this->app->runningInConsole()) {
            $this->commands([
                //Package Commands
                PublishCommand::class,
            ]);
        }

        $router = $this->app->make(Router::class);
        $router->aliasMiddleware('cors', Cors::class);
        $router->aliasMiddleware('json.response', ForceJsonResponse::class);
    }


    public function register()
    {
        $this->app->bind(
            IJWTService::class,
            JWTService::class
        );
    }
}
