
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//Sonmez/laravle-passport-auth routs
Route::post('login', [Sonmez\LaravelPassportAuth\Http\Controllers\Api\Auth\LoginController::class,'login'])->name('login');
Route::post('/password/email',[Sonmez\LaravelPassportAuth\Http\Controllers\Api\Auth\ForgotPasswordController::class,'sendResetLinkEmail'])->name('forgot-password');

Route::group(['middleware' => ['auth:api']], function () {
    Route::get('/logout', [Sonmez\LaravelPassportAuth\Http\Controllers\Api\Auth\LoginController::class,'logout'])->name('user.logout');
    Route::get('/token/validate', [Sonmez\LaravelPassportAuth\Http\Controllers\Api\Auth\AuthenticationController::class,'validateToken'])->name('passport.token.validate');
});
