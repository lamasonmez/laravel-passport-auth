#  Laravel Passport Authentication

You can use this package to implement Login and authentication functionalities on the fly relying on laravel passport package.

## Requirements

Since we are goingn to use autheticon of laravel we need to install laravel/ui package first

laravel/ui package


```bash
composer require laravel/ui
````

## Installation

1- You can install the package via composer:

```bash
composer require sonmez/laravel-passport-auth
```

2- After Successfull installation please run the following commands :

As mentioned in the laravel/passport documentation (see here https://laravel.com/docs/8.x/passport) the first thing we need to do after we have installed the package, is run the migrations:
 
```bash
php artisan migrate
```

With this command, you will have in your database all the laravel/passport tables (prefixed by oauth_) and the users table.
Now, we need to configure the laravel/passport package, by executing the next command:

```bash
php artisan passport:install
```
This command will generate 2 tokens in the oauth_clients, for my case I have the following tokens:

```bash
Personal access client created successfully.
Client ID: 1
Client secret: phNVmQWF0qxWrSw1thVzsx5B55w7Lm6cPUvDjgQo
Password grant client created successfully.
Client ID: 2
Client secret: srKHlpLcnyLaBhZmQsAIuztgY7C0N8gjZPFKjYgu
```
please take a copy of the second secret as we are going to need it cause we are implemnting the password protected authentication

After that, we need to configure our User model by adding the Laravel\Passport\HasApiTokens trait to it:

```
<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens; // <- HERE

class User extends Authenticatable
{
    use Notifiable, HasApiTokens; // <- HERE
    // ...
}
```



Then, we need to add Passport::routes() to the boot() function in the AuthServiceProvider

```
<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport; // <- HERE

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes(); // <- HERE
    }
}
```
then we are going to execute this command and this will publish the login and Authentication controller along with requried middelware and routes in the (api.php) route file

```bash
php artisan laravel-passport-auth:publish
```

You will notice that there is one folder  created inside your app folder "Api\Auth"  along with two **controllers**  
that has the login and logout with verify token 

last thing : remember we took a copy of CLient 2 secret you need to open .env file and copy the value to **PASSPORT_CLIENT_SECRET** field 
and now we are done 
you can test it by postman and make a post request to /api/login method as an example

